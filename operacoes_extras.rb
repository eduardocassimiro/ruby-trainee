module OperacoesExtras
  def verifica_fibonacci(numero)
    numero = numero.to_i
    if numero == 0    #Verifica a exceções do 0 e do 1
      return puts "É o 1º elemento na sequencia de Fibonacci."
    elsif numero == 1
      return puts "É o 2º ou 3º elemento na sequencia de Fibonacci."
    end

    n1 = 1    #Como já verificamos o 0 antes, não é necessário começar por ele
    n2 = 1
    ord = 3   #A contagem da começa do 3 pois na primeira iteração do laço ele já vai pra 4

    while true
      if n2 == numero     #Verifica se o número foi encontrado na sequência
        break
      elsif n2 > numero   #Caso o n2(soma mais recente) seja maior que o numero escolhido, significa que esse numero não está na sequência
        return puts "Nao faz parte da sequencia de Fibonacci."
      end
      aux = n2
      n2 = n1+n2
      n1 = aux
      ord+=1
    end
    return puts "É o "+ord.to_s+"º elemento na ordem de Fibonacci."
  end
end
