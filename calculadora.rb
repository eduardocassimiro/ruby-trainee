require_relative 'operacoes_extras'
require 'net/http'
require 'json'

module Calculadora
  class Operacoes
    include OperacoesExtras

    def media_preconceituosa(notas, lista_negra)
      notas = JSON.parse(notas)
      lista_negra = lista_negra.split(' ')

      lista_negra.each do |ln|    #Elimina as notas dos alunos na lista negra
        notas.delete(ln)
      end

      notas = notas.values
      puts notas.sum.to_f/notas.size.to_f   #Faz a media somente com os alunos remanescentes
    end

    def sem_numeros(numeros)
      numeros = numeros.split(' ')

      numeros.each do |num|
        if num[-1] == '5' and (num[-2] == '2' or num[-2] == '7') #Verifica se o número termina em 25 ou 75
          print 'S '
        elsif num[-1] == '0' and num[0] != '0' and (num[-2] == '0' or num[-2] == '5') #Verifica se o número termina em 00(e se não são só zeros) ou 50
          print 'S '
        else
          print 'N '
        end
      end
      puts
    end

    def filtrar_filmes(generos, ano)
      generos = generos.split(' ')

      filmes = get_filmes

      puts "Filmes-------------"

      generos.each do |g|   #Laço para alternar os valores de generos
        filmes[:movies].select do |a|    
          if (a[:genres].include? g and a[:year] >= ano)   #Verifica se possui o respectivo genero e se o ano é maior ou igual que o selecionado
            puts a[:title]
          end
        end
      end
    end
    
    private

    def get_filmes
      url = 'https://raw.githubusercontent.com/yegor-sytnyk/movies-list/master/db.json'
      uri = URI(url)
      response = Net::HTTP.get(uri)
      return JSON.parse(response, symbolize_names: true)
    end

  end

  class Menu
  
    def initialize
      ctrl = Operacoes.new
      puts " -------------------------"
      puts "| Bem vindo a calculadora |"
      puts " -------------------------"

      puts "1. Média Preconceituosa"
      puts "2. Divisíveis por 25"
      puts "3. Filtro de Filmes"
      puts "4. Sequencia de Fibonacci"

      puts " -------------------------"
      print "Sua opção:"
      item = gets.chomp
      puts " -------------------------"
      if item == "1"
        puts "   Média Preconceituosa"
        puts " -------------------------"

        print "Notas: "
        notas = gets.chomp
        print "Lista Negra: "
        lista_negra = gets.chomp
        ctrl.media_preconceituosa(notas, lista_negra)

      elsif item == "2"
        puts "    Divisíveis por 25"
        puts " -------------------------"

        print "Numeros: "
        numeros = gets.chomp
        ctrl.sem_numeros(numeros)

      elsif item == "3"
        puts "     Filtro de Filmes"
        puts " -------------------------"

        print "Generos: "
        generos = gets.chomp
        print "Ano: "
        ano = gets.chomp
        ctrl.filtrar_filmes(generos, ano)
      
      elsif item == "4"
        puts "  Sequencia de Fibonacci"
        puts " -------------------------"
        puts "Verifica se o numero faz parte da sequencia de Fibonacci indicando sua ordem."
        
        print "Numero: "
        numero = gets.chomp
        ctrl.verifica_fibonacci(numero)
      end
    end
  
  end

end
